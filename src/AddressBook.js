import React from 'react';

const swissPhoneRegex = /^(?:(?:|0{1,2}|\+{0,2})41(?:|\(0\))|0)([1-9]\d)(\d{3})(\d{2})(\d{2})$/;

const clearForm = () => {
  document.getElementById('my-form').reset();
};

const MAXIMUM = 10;

function App() {
  const [entries, setEntries] = React.useState([]);
  const [numberInvalid, setNumberInvalid] = React.useState(false);
  const maxReached = React.useMemo(() => entries.length === MAXIMUM, [entries]);
  return (
    <div>
      <h1>Address book</h1>
      <form
        style={{ display: 'flex', flexDirection: 'column', width: '300px' }}
        id="my-form"
        onSubmit={(e) => {
          e.preventDefault();
          if (maxReached) {
            return;
          }
          const {
            target: [{ value: name }, { value: phoneNo }, { value: address }],
          } = e;

          if (swissPhoneRegex.test(phoneNo)) {
            setNumberInvalid(false);
            setEntries((pv) => [...pv, { name, phoneNo, address }]);
            clearForm();
          } else {
            setNumberInvalid(true);
          }
        }}
      >
        <label htmlFor="name">name:</label>
        <input type="text" id="name" />
        <br />
        <label htmlFor="phone-number">number:</label>
        <input type="tel" id="phone-number" />
        {numberInvalid && 'Number is invalid'}
        <br />
        <label htmlFor="address">address:</label>
        <input type="text" id="address" />
        <br />
        <button type="submit">Submit</button>
      </form>
      <hr />
      {maxReached && 'Reached maximum'}
      {entries.map(({ address, name, phoneNo }) => (
        <div key={name + address}>
          Name: {name}, Phone number: {phoneNo}, Address: {address}
        </div>
      ))}
    </div>
  );
}

export default App;
