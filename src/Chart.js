import React from 'react';

const Chart = () => {
  React.useEffect(() => {
    window.Highcharts.chart('container', {
      title: {
        text: 'Chart showing number of sales',
      },

      yAxis: {
        title: {
          text: 'Value of sales in $',
        },
      },

      xAxis: {
        minorTickInterval: 0.1,
        title: {
          text: 'Time',
        },
        accessibility: {
          rangeDescription: 'Range: 1980 to 2000',
        },
      },

      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
      },

      plotOptions: {
        series: {
          label: {
            connectorAllowed: false,
          },
          pointStart: 1980,
        },
      },

      series: [
        {
          name: 'Sales in $',
          data: [
            [1980, 14000],
            [1985, 22000],
            [1990, 104000],
            [1995, 87000],
            [2000, 43400],
          ],
        },
      ],

      responsive: {
        rules: [
          {
            condition: {
              maxWidth: 500,
            },
            chartOptions: {
              legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom',
              },
            },
          },
        ],
      },
    });
  }, []);
  return (
    <>
      <h1>Chart</h1>

      <figure className="highcharts-figure">
        <div id="container"></div>
      </figure>
    </>
  );
};

export default Chart;
