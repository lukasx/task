import React from 'react';
import ReactDOM from 'react-dom';
import AddressBook from './AddressBook';
import Chart from './Chart';

ReactDOM.render(<AddressBook />, document.getElementById('address-book'));
ReactDOM.render(<Chart />, document.getElementById('chart'));
